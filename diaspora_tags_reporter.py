#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from lxml import html
from lxml.html import builder
from pathlib import Path

import aiohttp
import asyncio
import json

class MessagesNotLoadedException(Exception):
    """ Custom exception raised when you try to generate the report before loading the messages """
    pass

class MessagesAlreadyReactedException(Exception):
    """ Custom exception raised when you already have reacted to a message """
    pass

class DiasporaTagsReporter():
    """ Retrieves all messages related to tags defined in the configuration file. """
    def __init__(self, ignore, pods, report, tags):
        """ Constructor """
        self._ignore_ids = set(ignore)
        self._pods = pods
        self._diaspora_id = None
        self._pod_name = None
        self._min_date = None
        self._tags = tags
        self._report_file = get_current_dir() / report
        self._msgs = None

    async def _fetch_json(self, url):
        """ Download json data corresponding to an url in async way """
        try:
            async with aiohttp.ClientSession() as session:
                async with session.get(url) as response:
                    if response.status == 200:
                        return await response.json()
                    else:
                        return []
        except:
            return []

    def _get_messages_from_ids(self, guids):
        """ Get the messages corresponding to the given ids """
        urls = set()
        msgs = []
        guids = set(guids)
        for guid in guids:
            urls.add('https://{}/posts/{}.json'.format(self._pod_name,guid))
        loop = asyncio.get_event_loop()
        data = loop.run_until_complete(asyncio.gather(*(self._fetch_json(url) for url in urls)))
        for msg_data in data:
            try:
                if msg_data == []:
                    continue
                msgs.append({
                    'author' : '{} ({})'.format(msg_data['author']['name'],msg_data['author']['diaspora_id']),
                    'text' : msg_data['text'],
                    'nsfw' : msg_data['nsfw'],
                    'created_at' : msg_data['created_at'][:-5],
                    'url' : 'https://{}/posts/{}'.format(self._pod_name,msg_data['guid']),
                    'reactors' : list(self._get_message_reactors(msg_data))
                })
            except MessagesAlreadyReactedException:
                continue
        return msgs

    def _get_message_ids_from_tags(self, tags):
        """ Get the message ids for all messages published with the givent tags """
        urls = set()
        msg_ids = set()
        for tag in tags:
            urls.add('https://{}/tags/{}.json'.format(self._pod_name,tag))
        loop = asyncio.get_event_loop()
        data = loop.run_until_complete(asyncio.gather(*(self._fetch_json(url) for url in urls)))
        for msgs in data:
            for msg_data in msgs:
                msg_date = datetime.strptime(msg_data['created_at'][:-5],'%Y-%m-%dT%H:%M:%S')
                if msg_date < self._min_date:
                    continue
                if msg_data['author']['diaspora_id'] in self._ignore_ids:
                    continue
                if msg_data['author']['diaspora_id'] == self._diaspora_id:
                    continue
                msg_ids.add(msg_data['guid'])
        return msg_ids

    def _get_message_reactors(self, msg_data):
        """ Get the list of reactors for a message """
        reactors = set()
        for cat in msg_data['interactions']:
            try:
                for reactor in msg_data['interactions'][cat]:
                    if reactor['author']['diaspora_id'] == self._diaspora_id:
                        raise MessagesAlreadyReactedException()
                    reactors.add('{} ({})'.format(reactor['author']['name'],reactor['author']['diaspora_id']))
            except TypeError:
                pass
        return reactors

    def load_messages(self):
        """ Loads the messages corresponding to the tags defined in the configuration file """
        self._msgs = {}
        for pod in self._pods:
            self._msgs[pod['name']] = {}
            self._diaspora_id = pod['diaspora_id']
            self._pod_name = pod['name']
            self._min_date = datetime.utcnow() - timedelta(days=pod['max_days'])
            for cat in self._tags:
                tags_to_explore = self._tags[cat]
                msg_ids = self._get_message_ids_from_tags(tags_to_explore)
                self._msgs[pod['name']][cat] = self._get_messages_from_ids(msg_ids)

    def save_report(self):
        """ Generate the report """
        # generate document
        doc = builder.HTML()
        # generate header
        title = "Diaspora tag reporter"
        head = builder.HEAD()
        meta = builder.META()
        meta.set('charset','UTF-8')
        head.append(meta)
        link = builder.LINK()
        link.set('rel','stylesheet')
        link.set('type','text/css')
        link.set('href','https://www.w3schools.com/w3css/4/w3.css')
        head.append(link)
        head.append(builder.TITLE(title))
        doc.append(head)
        # generate body
        body = builder.BODY()
        div = builder.DIV()
        div.set('class','w3-container')
        h1 = builder.H1(title)
        div.append(h1)
        body.append(div)
        for pod_name in self._msgs:
            msgs = self._msgs[pod_name]
            if msgs is None:
                raise MessagesNotLoadedException('Messages not loaded')
            title = "Tag report for {}".format(pod_name)
            div = builder.DIV()
            div.set('class','w3-container')
            h2 = builder.H2(title)
            h2.set('class','w3-teal')
            div.append(h2)
            body.append(div)
            for cat in msgs:
                div = builder.DIV()
                div.set('class','w3-container')
                h3 = builder.H2(cat)
                h3.set('class','w3-amber')
                div.append(h3)
                for msg in msgs[cat]:
                    panel = builder.DIV()
                    panel.set('class','w3-panel w3-sand')
                    ul = builder.UL()
                    ul.set('class','w3-ul')
                    for key in msg:
                        li = builder.LI('{}: '.format(key))
                        if key == 'url':
                            a = builder.A(msg[key])
                            a.set('href',msg[key])
                            a.set('target','_blank')
                            a.set('class','w3-text-indigo')
                            s = builder.STRONG()
                            s.append(a)
                            li.append(s)
                        elif key == 'nsfw':
                            if msg[key]:
                                span = builder.SPAN(str(msg[key]))
                                span.set('class','w3-red w3-padding-small w3-round')
                                li.append(builder.STRONG(span))
                            else:
                                li.append(builder.STRONG(str(msg[key])))
                        elif key == 'text':
                            card = builder.DIV()
                            card.set('class','w3-card w3-padding w3-pale-yellow')
                            lines = msg[key].split('\n\r')
                            for line in lines:
                                if len(line) > 0:
                                    card.append(builder.SPAN(line))
                                    card.append(builder.BR())
                            li.append(card)
                        elif key == 'reacters' and len(msg[key]) == 0:
                            li.append(builder.STRONG('---'))
                        else:
                            li.append(builder.STRONG(str(msg[key])))
                        ul.append(li)
                    panel.append(ul)
                    div.append(panel)
                body.append(div)
            doc.append(body)
        # save the report
        with self._report_file.open('w',encoding='utf-8') as fout:
            fout.write('<!DOCTYPE html>\n')
            fout.write(html.tostring(doc, pretty_print=True).decode('utf-8'))

def get_current_dir():
    """ Retrurn the directory of the script """
    return Path(__file__).parent


def load_config(filename):
    """ Load a configuration file in the current directory """
    file_to_load = get_current_dir() / filename
    with file_to_load.open('r',encoding='utf-8') as fin:
        return json.load(fin)


def Main():
    """ Entry script """
    config = load_config('config.json')
    dtr = DiasporaTagsReporter(**config)
    dtr.load_messages()
    dtr.save_report()

if __name__ == '__main__':
    Main()

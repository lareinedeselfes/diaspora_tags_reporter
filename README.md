ceci est un Fork du projet de Sébastien Adam que je remercie 


# Diaspora Tags Reporter

[Version française](LISEZMOI.md).

Since my English is limited, this text has been translated from French with [DeepL](https://www.deepl.com/translator).

## Preamble

I use this script to list messages from newcomers on the [diaspora*](https://diasporafoundation.org/) social network and welcome them. However, you can configure it and use it to retrieve messages about other topics.

The script was written in [Python 3](https://www.python.org/downloads/) and uses the following libraries:

* [aiohttp](https://aiohttp.readthedocs.io/en/stable/)
* [asyncio](https://docs.python.org/3/library/asyncio.html)
* [lxml](http://lxml.de/)

Here is an overview of the result:

![screenshot](https://framagit.org/sebastienadam/diaspora_tags_reporter/raw/develop/screenshot.png)

## Usage

Execution of the script will generate an HTML page with messages containing a list of given #tags. You can set the maximum age (in days) of messages to report. Also, the messages you have responded to will not be listed. Finally, you can define a list of users to ignore.

### Setup

The configuration is done using the [config.json](https://framagit.org/sebastienadam/diaspora_tags_reporter/blob/develop/config.json) file. It is in [JSON](http://json.org/) format. This is its structure:

````
{
  "ignore" : [
    "<diaspora_id_to_ignore>",
    "<diaspora_id_to_ignore>"
  ],
  "pod": [{
    "diaspora_id" : "sebastienadam@framasphere.org",
    "max_days" : 5,
    "name" : "framasphere.org"
  },{
    "diaspora_id" : "sebastienadam@diasp.de",
    "max_days" : 5,
    "name" : "diasp.de"
  }],
  "report" : "report.html",
  "tags" : {
    "Newcomers" : ["newhere","new-here","new_here"],
    "Help needed" : ["question","help"]
  }
}
````

The `ignore' entry contains the list of identifiers of people to be ignored.

The `pods` entry contains the list of the servers to explore with these settings:

* `diaspora_id`: your diaspora* identifier (used to ignore messages you have already responded to).
* `max_days`: maximum age, in days, of the messages to be retrieved. Older messages will be ignored.
* `name`: the domain name of your pod. Do not give the full address. For example, if the address of your pod is "https://framasphere.org", you should only give "framasphere. org".

The `report` entry contains the name of the result file.

The `tags` entry contains the list of #tags included in the messages to retrieve. The entries are in `<label>:[<tag>, <tag>,...]` format. The report will group messages based on the `<label>` and containing at least one of the `#<tag>` of the list. The above example shows two entries: one for newcomers and one for those who ask a question. You can use this setting to define other message types to be retrieved.

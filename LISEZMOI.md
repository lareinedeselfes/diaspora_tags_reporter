ceci est un fork du projet de Sébastien Adam que je remercie 

# Diaspora Tags Reporter

[English version](README.md)

## Préambule

J'utilise ce script pour lister les messages des nouveaux arrivants sur le réseau social [diaspora*](https://diasporafoundation.org/) et ainsi leur souhaiter la bienvenue. Vous pouvez toutefois le configurer et l'utiliser pour récupérer des messages concernant d'autres sujet.

Le script a été écrit en [Python 3](https://www.python.org/downloads/) et utilise les librairies suivantes:

* [aiohttp](https://aiohttp.readthedocs.io/en/stable/)
* [asyncio](https://docs.python.org/3/library/asyncio.html)
* [lxml](http://lxml.de/)

Voici un aperçu du résultat:

![screenshot](https://framagit.org/sebastienadam/diaspora_tags_reporter/raw/develop/screenshot.png)

## Utilisation

L'exécution du script va générer une page HTML reprenant les messages contenant une liste de #tags donnés. Vous pouvez définir l'âge maximum (en jours) des messages à rapporter. Aussi, les messages auxquels vous avez réagit ne seront pas listés. Enfin, vous pouvez définir une liste d'utilisateurs à ignorer.

### Configuration

La configuration se fait grâce au fichier [config.json](https://framagit.org/sebastienadam/diaspora_tags_reporter/blob/develop/config.json). Il est au format [JSON](http://json.org/). Voici sa structure:

````
{
  "ignore" : [
    "<diaspora_id_to_ignore>",
    "<diaspora_id_to_ignore>"
  ],
  "pods": [{
    "diaspora_id" : "sebastienadam@framasphere.org",
    "max_days" : 5,
    "name" : "framasphere.org"
  },{
    "diaspora_id" : "sebastienadam@diasp.de",
    "max_days" : 5,
    "name" : "diasp.de"
  }],
  "report" : "report.html",
  "tags" : {
    "Nouveaux" : ["nouveauici","nouvelleici","nouveau","nouvelle","nouveau-ici","nouvelle-ici"],
    "Questions" : ["question","aide"]
  }
}
````

L'entrée `ignore` contient la liste des identifiants des personnes à ignorer.

L'entrée `pods` contient la liste des serveurs à explorer avec les paramètres suivants:

* `diaspora_id`: votre identifiant diaspora* (utilisé pour ignorer les messages auxquels vous avez déjà réagit).
* `max_days`: âge maximum, en jours, des messages à récupérer. Les messages plus anciens seront ignorés.
* `name`: le nom de domaine de votre pod. Il ne faut pas donner l'adresse complète. Ainsi, si l'adresse de votre pod est "https://framasphere.org", vous ne devez donner que "framasphere.org".

L'entrée `report` contient le nom du fichier de résultat.

L'entrée `tags` contient la liste des #tags contenus dans les messages à récupérer. Les entrées sont au format `<label>:[<tag>,<tag>,...]`. Le rapport groupera les messages sur base des `<label>` et contenant au moins un des `#<tag>` de la liste. L'exemple ci-dessus montre deux entrées: une pour les nouveaux arrivants et une pour ceux qui posent une question. Vous pouvez utiliser ce paramètre pour définir d'autres catégories de messages  à récupérer.
